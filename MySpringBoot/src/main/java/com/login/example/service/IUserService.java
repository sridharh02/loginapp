package com.login.example.service;

import java.util.List;

import com.login.example.model.UserDetails;

public interface IUserService {

	public void addUser(String username, String password);
	public List<UserDetails> findUserByName(String name, String password);
	
}
