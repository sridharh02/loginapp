package com.login.example.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.login.example.model.UserDetails;
import com.login.example.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository repo;
	
	@Override
	public List<UserDetails> findUserByName(String username, String password) {
		System.out.println("inside service findUserByName()");
		UserDetails user = new UserDetails();
		user.setUsername(username);
		user.setPassword(password);
		return repo.findAll();
	}

	@Override
	public void addUser(String username, String password) {
		UserDetails user = new UserDetails();
		user.setUsername(username);
		user.setPassword(password);
		repo.save(user);
	}
}
