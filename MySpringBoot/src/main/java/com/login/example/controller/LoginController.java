package com.login.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.login.example.model.UserDetails;
import com.login.example.service.IUserService;

@RestController
@RequestMapping("/api")
public class LoginController {
	
	@Autowired
	private IUserService service;
	
	@GetMapping("/login")
	public ResponseEntity<String> doLogin(@RequestParam("userName") String userName, 
					@RequestParam("password") String password) {
		System.out.println("inside LoginController");
		List<UserDetails> users = service.findUserByName(userName, password);
		if(users.size() > 0)
			return new ResponseEntity<String>(String.format("Welcome Mr. %s", userName), HttpStatus.OK);
		else
			return new ResponseEntity<String>(String.format("Invalid user Mr. %s", userName), HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/greet")
	public ResponseEntity<String> greetMethod() {
		return new ResponseEntity<String>(String.format("Hello there!!!"), HttpStatus.OK);
	}
}
