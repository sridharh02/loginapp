package com.login.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.login.example.model.UserDetails;

@Repository
public interface UserRepository extends JpaRepository<UserDetails, Integer>{

}
