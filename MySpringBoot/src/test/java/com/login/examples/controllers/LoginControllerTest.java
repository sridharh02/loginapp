package com.login.examples.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.login.example.MySpringBoot.MySpringBootApplication;
import com.login.example.service.UserServiceImpl;

@WebMvcTest
@ContextConfiguration(classes=MySpringBootApplication.class)
@RunWith(SpringRunner.class)
public class LoginControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Mock
	private UserServiceImpl service;
	
	@Test
	public void positiveCase() throws Exception {
		String userName = "sridhar";
		String password = "sridhar";
		
		mockMvc.perform(
			get("/api/login")
			.param("userName", userName)
			.param("password", password)
		).andExpect(status().isOk())
		.andExpect(content().string(containsString(String.format("Welcome Mr. %s", userName))));
	}
	
	@Test
	public void negativeCase() throws Exception {
		String userName = "abc";
		String password = "abc";
		
		mockMvc.perform(
			get("/api/login")
			.param("userName", userName)
			.param("password", password)
		).andExpect(status().isNotFound())
		.andExpect(content().string(containsString(String.format("Invalid user Mr. %s", userName))));
	}
}
